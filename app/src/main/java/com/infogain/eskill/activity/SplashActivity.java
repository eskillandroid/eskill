package com.infogain.eskill.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.infogain.eskill.R;
import com.infogain.eskill.constants.DataController;
import com.infogain.eskill.model.LoginData;
import com.infogain.eskill.util.AppUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class SplashActivity extends BaseActivity {


    public static final String MyPREFERENCES = "MyPrefs";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
         AppUtils.LogAnalyticsEvent("Splash Screen", FirebaseAnalytics.Event.APP_OPEN, getApplicationContext());

         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            getWindow().getAttributes().layoutInDisplayCutoutMode = WindowManager.LayoutParams.LAYOUT_IN_DISPLAY_CUTOUT_MODE_SHORT_EDGES;
        } else
        {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        setContentView(R.layout.activity_splash);
        final Handler handler = new Handler();

           handler.postDelayed(new Runnable() {
                                   @Override
                                public void run() {
                                 fetchUserDefaultsDetail();
                                }
                               }
                   , 3000);
    }
    private void fetchUserDefaultsDetail() {

        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);boolean islogIn = prefs.getBoolean("ISLOGIN", false);

        //responce
    if (islogIn) {

        String response = prefs.getString("response", null);

        try {

            JSONObject jsonObject = new JSONObject(response);

            LoginData loginData=   new LoginData(jsonObject);

            DataController.getInstance().setLogin(loginData);

            Intent i = new Intent(SplashActivity.this, DashboardActivity.class);

            startActivity(i);

            finish();
        }

        catch (JSONException e)
         {
          }
    } else {
            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
            startActivity(i);
            finish();
       }
    }
}
