package com.infogain.eskill.activity;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
//import com.crashlytics.android.Crashlytics;
import com.github.pavlospt.CircleView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.infogain.eskill.EskillApplication;
import com.infogain.eskill.R;
import com.infogain.eskill.constants.AppConstants;
import com.infogain.eskill.constants.DataController;
import com.infogain.eskill.constants.FusionChart;
import com.infogain.eskill.model.OrganizationData;
import com.infogain.eskill.ssl_certificates.Sslcertificates;
import com.infogain.eskill.util.ActionSheetCustom;
import com.infogain.eskill.util.StringOperations;
import com.infogain.eskill.util.AppUtils;
import com.infogain.eskill.widgets.CircularTextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inf-mobility on 29-06-2017.
 */

public class DashboardActivity extends BaseActivity implements ActionSheetCustom.ActionSheetListener, View.OnClickListener {

    public static final String MyPREFERENCES = "MyPrefs";
    private LinearLayout ll_filterbox2;
    private TextView tv_primarySkill;
    private TextView tv_sbu;
    private EditText et_search_term;
    private TextView search_btn;
    private TextView btn_reset;

    private ActionSheetCustom.Builder actionSheet;

    private String[] optionFields;

    private ArrayList<String> filterValues;

    private boolean mPrimary_Selected = true;

    private boolean mSecondary_Selected = false;

    private ArrayList<OrganizationData.KeysData> primaryOptions;

    private ArrayList<OrganizationData.KeysData> secondaryOptions;


    private CircularTextView nameCircularTv;
    private WebView webview;


    private String selectedSBU = "";

    private String selectedPrimarySkill = "";

    private TextView tv_searchPrimary;

    private TextView tv_searchOptionPrimary;
    private TextView nameTv;
    private String mSelectedPrimary;

    private String selectedPrimaryValue = "";

    private String selectedPrimaryOptionValue = "";

    private TextView tv_searchSecondary;

    private TextView tv_searchOptionSecondary;

    private String mSelectedSecondary;

    private String selectedSecondaryValue = "";

    private String selectedSecondaryOptionValue = "";

    private RequestQueue requestQueue;

    private FrameLayout fl_loader;

    boolean doubleBackToExitPressedOnce = false;
    Animation slideUpAnimation;
    TextView logoutTV;
    boolean sheetOpen = false;
    com.github.pavlospt.CircleView circleView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);



        Sslcertificates sslcertificates = new Sslcertificates();
        sslcertificates.appTrustManger();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);



        AppUtils.LogAnalyticsEvent("Dashboard Screen", FirebaseAnalytics.Event.VIEW_ITEM, getApplicationContext());

        webview = (WebView) findViewById(R.id.webview);
        webview.getSettings().setJavaScriptEnabled(true);
        webview.setWebChromeClient(new WebChromeClient());
        filterValues = DataController.getInstance().getLogin().getOrganizationData().getFilterValues();
        optionFields = DataController.getInstance().getLogin().getOrganizationData().getFilterNames();
        primaryOptions = new ArrayList<>();
        secondaryOptions = new ArrayList<>();
        initViews();
        mSelectedPrimary = "Designation";
        primaryOptions = DataController.getInstance().getLogin().getOrganizationData().getDesignation();
        tv_searchPrimary.setText(mSelectedPrimary);
        tv_searchOptionPrimary.setText("Optional");
        selectedPrimaryValue = filterValues.get(3);
        setupListeners();
        requestQueue = EskillApplication.getInstance().getRequestQueue();
        webview.loadDataWithBaseURL("file:///android_asset/", FusionChart.get2DPieChart(DataController.getInstance().getLogin().fusionDataRaw), "text/html", "UTF-8", null);

        slideUpAnimation = AnimationUtils.loadAnimation(getApplicationContext(),
                R.anim.slide_up_animation);
        SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
        String name = prefs.getString("name", null);
          if (name != null)
          {
              nameTv.setText(name);
              circleView.setTitleText(""+name.charAt(0));

          }

    }

    private void initViews() {
       // nameCircularTv = (CircularTextView) findViewById(R.id.nameCircularTv);
        circleView = (CircleView)  findViewById(R.id.circleView);
        //circleView.setTitleText("B");

        nameTv = (TextView) findViewById(R.id.nameTV);

        logoutTV =   (TextView) findViewById(R.id.logoutTV);
        fl_loader =  (FrameLayout) findViewById(R.id.flLoader);
        ll_filterbox2 = (LinearLayout) findViewById(R.id.filterBox2);
        btn_reset =     (TextView) findViewById(R.id.reset_btn);
        search_btn =    (TextView) findViewById(R.id.search_btn);
        et_search_term = (EditText) findViewById(R.id.et_search_term);
        tv_searchPrimary = (TextView) findViewById(R.id.searchPrimary);
        tv_searchSecondary = (TextView) findViewById(R.id.searchSecondary);
        tv_searchOptionPrimary = (TextView) findViewById(R.id.searchOptionsPrimary);
        tv_searchOptionSecondary = (TextView) findViewById(R.id.searchOptionsSecondary);
        tv_primarySkill =  (TextView) findViewById(R.id.tv_primary_skill);
        tv_sbu =       (TextView)          findViewById(R.id.tv_sbu);
      /*  nameCircularTv.setStrokeWidth(1);
        nameCircularTv.setStrokeColor("#ffffff");
        nameCircularTv.setSolidColor("#000000");*/
    }

    private void setupListeners() {

        btn_reset.setOnClickListener(this);

        circleView.setOnClickListener(this);
        search_btn.setOnClickListener(this);

        tv_searchOptionPrimary.setOnClickListener(this);

        tv_searchOptionSecondary.setOnClickListener(this);

        tv_searchPrimary.setOnClickListener(this);

        tv_searchSecondary.setOnClickListener(this);

        tv_sbu.setOnClickListener(this);

        tv_primarySkill.setOnClickListener(this);

        logoutTV.setOnClickListener(this);
    }

    private void changeActiveState(boolean state) {

        tv_primarySkill.setEnabled(state);

        tv_sbu.setEnabled(state);

        et_search_term.setEnabled(state);

        search_btn.setEnabled(state);

        btn_reset.setEnabled(state);

        tv_searchPrimary.setEnabled(state);

        tv_searchOptionPrimary.setEnabled(state);

        tv_searchSecondary.setEnabled(state);

        tv_searchOptionSecondary.setEnabled(state);

        webview.setEnabled(state);
    }

    void getCustomSearch() {

        fl_loader.setVisibility(View.VISIBLE);

        changeActiveState(false);

        requestQueue = Volley.newRequestQueue(DashboardActivity.this);
        // Use stringRequest instread of array or object

        String searchTerm = et_search_term.getText().toString();

        JSONObject jsonobject_one = new JSONObject();

        try {
            jsonobject_one.put("searchTerm", "");

            jsonobject_one.put("sbu", "");

            jsonobject_one.put("skill", "");

            jsonobject_one.put("primaryCol1", selectedPrimaryValue);

            jsonobject_one.put("primaryCol2", selectedSecondaryValue);

            jsonobject_one.put("secondaryCol1", selectedPrimaryOptionValue);

            jsonobject_one.put("secondaryCol2", selectedSecondaryOptionValue);

        } catch (JSONException e) {

            Log.e("ObjectCreation", "error: " + e.toString());

        }

        Log.e("post: ", "" + jsonobject_one);

        Log.e("TAG: ", "" + jsonobject_one);

        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.PRIMARY_SEARCH_URL, jsonobject_one, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                fl_loader.setVisibility(View.GONE);

                changeActiveState(true);

                Log.e("TAG", "responce" + response.toString());

                String jsonString;

                jsonString = response.optJSONArray("fusionData").toString();

                webview.loadDataWithBaseURL("file:///android_asset/", FusionChart.get2DPieChart(jsonString), "text/html", "UTF-8", null);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                fl_loader.setVisibility(View.GONE);

                changeActiveState(true);

                Log.e("error", "error" + error.toString());

            }
        });


        objectRequest.setRetryPolicy(new DefaultRetryPolicy(


                9000,

                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,

                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        requestQueue.add(objectRequest);
    }

    void getGraphDataSearch() {

        Log.e("TAG", "in method ");
        fl_loader.setVisibility(View.VISIBLE);

        changeActiveState(false);

        requestQueue = Volley.newRequestQueue(DashboardActivity.this);
        // Use stringRequest instread of array or object
        String searchTerm = et_search_term.getText().toString();

        JSONObject jsonobject_one = new JSONObject();

        try {

            jsonobject_one.put("searchTerm", searchTerm);

            jsonobject_one.put("sbu", selectedSBU);

            jsonobject_one.put("skill", selectedPrimarySkill);

        } catch (JSONException e) {

            Log.e("TAG", "error: " + e.toString());

        }

        Log.e("TAG: ", "onpost " + jsonobject_one);

        JsonObjectRequest objectRequest = new JsonObjectRequest(Request.Method.POST, AppConstants.BASE_URL + AppConstants.GRAPH_URL, jsonobject_one, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                fl_loader.setVisibility(View.GONE);

                changeActiveState(true);

                Log.e("TAG", "responce" + response.toString());

                String jsonString = null;

                jsonString = response.optJSONObject("fusionData").optJSONArray("designation").toString();

                webview.loadDataWithBaseURL("file:///android_asset/", FusionChart.get2DPieChart(jsonString), "text/html", "UTF-8", null);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                fl_loader.setVisibility(View.GONE);

                changeActiveState(true);

                Log.e("error", "error" + error.toString());
            }
        });

        objectRequest.setRetryPolicy(new DefaultRetryPolicy(
                9000,

                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,

                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        requestQueue.add(objectRequest);
    }


    void getGraphData() {

        fl_loader.setVisibility(View.VISIBLE);

        changeActiveState(false);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, AppConstants.BASE_URL + AppConstants.GRAPH_ORG_SEARCH_URL,

                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        fl_loader.setVisibility(View.GONE);

                        changeActiveState(true);

                        Log.e("Response", response);

                        JSONObject jsonObject = null;
                        try {

                            jsonObject = new JSONObject(response);

                        } catch (JSONException e) {

                            e.printStackTrace();

                        }

                        String jsonString = null;

                        jsonString = jsonObject.optJSONObject("fusionData").optJSONArray("designation").toString();

                        webview.loadDataWithBaseURL("file:///android_asset/", FusionChart.get2DPieChart(jsonString), "text/html", "UTF-8", null);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        fl_loader.setVisibility(View.GONE);

                        changeActiveState(true);

                        Log.e("Response", error.toString());
                    }
                }) {

        };

        requestQueue.add(stringRequest);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.searchPrimary:
                showActionSheetPrimary(AppConstants.TYPE_PRIMARY);
                break;
            case R.id.searchSecondary:
                showActionSheetPrimary(AppConstants.TYPE_SECONDARY);
                break;
            case R.id.searchOptionsPrimary:
                showActionSheetOptions(primaryOptions, AppConstants.TYPE_PRIMARY_OPTION);
                break;
            case R.id.searchOptionsSecondary:
                showActionSheetOptions(secondaryOptions, AppConstants.TYPE_SECONDARY_OPTION);
                break;
            case R.id.search_btn:
                AppUtils.LogAnalyticsEvent("Search", FirebaseAnalytics.Event.SELECT_CONTENT, getApplicationContext());
                if (et_search_term.getText().toString().equals("") && selectedSBU.equals("") && selectedPrimarySkill.equals("")) {
                    StringOperations.shortToast("Fill at least 1 field", DashboardActivity.this);
                    break;
                }

                getGraphDataSearch();
                break;
            case R.id.reset_btn:

     //           Crashlytics.getInstance().crash();
                resetData();
                getGraphData();
                break;
            case R.id.tv_primary_skill:
                showActionSheetSkill();
                break;
            case R.id.tv_sbu:
                showActionSheetSBU();
                break;
            case R.id.circleView:
                @SuppressLint("RestrictedApi") final Dialog dialog = new Dialog(new ContextThemeWrapper(this, R.style.DialogSlideAnim));
                // Include dialog.xml file
                getWindow().setGravity(Gravity.BOTTOM);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
                dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
                dialog.setContentView(R.layout.logout_dialog);


                CircleView circularTextView = (CircleView) dialog.findViewById(R.id.circleView);

                TextView nameTv = (TextView) dialog.findViewById(R.id.nameTV);
                TextView emailTv = (TextView) dialog.findViewById(R.id.emailTv);

                SharedPreferences prefs = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);
                String name = prefs.getString("name", null);

                // String email = prefs.getString("email", null);
                if (name != null) {
                    nameTv.setText(name);
                    circularTextView.setTitleText(""+name.charAt(0));
                   // emailTv.setText(name+"@infogain.com".replace(" ",""));

                }
               /* if (email != null || !email.equals("") || !email.equals(" "))
                {
                    emailTv.setText(email);
                }*/
                // Set dialog title
                dialog.setTitle("Custom Dialog");

                Button btn_logout = (Button) dialog.findViewById(R.id.btn_logout);

                btn_logout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit();
                        editor.putBoolean("ISLOGIN", false);
                        editor.putString("response","");
                        editor.putString("name","");
                        editor.commit();

                        Intent i = new Intent(DashboardActivity.this, LoginActivity.class);
                        startActivity(i);
                        finish();

                    }
                });
                // set values for custom dialog components - text, image and button
              /*  TextView text = (TextView) dialog.findViewById(R.id.textDialog);
                text.setText("Custom dialog Android example.");
                ImageView image = (ImageView) dialog.findViewById(R.id.imageDialog);
                image.setImageResource(R.drawable.image0);*/

                 dialog.show();

              /*  Button declineButton = (Button) dialog.findViewById(R.id.declineButton);
                // if decline button is clicked, close the custom dialog
                declineButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // Close dialog
                        dialog.dismiss();
                    }
                });*/







                break;

            case R.id. logoutTV:

                SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit();
                editor.putBoolean("ISLOGIN", false);
                editor.putString("response","");
                editor.putString("name","");
                editor.commit();

                Intent i = new Intent(DashboardActivity.this, LoginActivity.class);
                startActivity(i);
                finish();

                break;

        }
    }


    private void resetData() {
        selectedSBU = "";
        selectedPrimarySkill = "";
        tv_sbu.setText("SBU");
        tv_primarySkill.setText("Primary Skill");
        et_search_term.setText("");
        et_search_term.setHint("Search Term (Inside Resume)");

        tv_primarySkill.setTextColor(getResources().getColor(R.color.light_gray));
        tv_sbu.setTextColor(getResources().getColor(R.color.light_gray));

        tv_searchOptionPrimary.setText("Optional");
        tv_searchOptionSecondary.setText("Select");
        ll_filterbox2.setVisibility(View.GONE);

        mPrimary_Selected = true;
        mSecondary_Selected = false;

        mSelectedSecondary = "";
        mSelectedPrimary = "Designation";

        tv_searchPrimary.setText(mSelectedPrimary);
        tv_searchSecondary.setText(mSelectedSecondary);
        tv_searchSecondary.setText("Select");

        selectedPrimaryValue = filterValues.get(3);
        selectedSecondaryValue = "";
        selectedPrimaryOptionValue = "";
        selectedSecondaryOptionValue = "";
        secondaryOptions = null;
        primaryOptions = null;


    }


    private void showActionSheetOptions(ArrayList<OrganizationData.KeysData> keysData, String sheetType) {
        sheetOpen = true;
        actionSheet = new ActionSheetCustom.Builder(DashboardActivity.this, getSupportFragmentManager());
        actionSheet.setTag(sheetType);
        actionSheet.setCancelButtonTitle("Cancel");

        String[] optionType = new String[keysData.size()];
        for (int index = 0; index < keysData.size(); index++) {
            optionType[index] = keysData.get(index).getValue();
        }

        actionSheet.setOtherButtonTitles(optionType);
        actionSheet.setCancelableOnTouchOutside(true);
        actionSheet.setListener(this);
        actionSheet.show();
    }

    private void showActionSheetSkill() {
        sheetOpen = true;
        actionSheet = new ActionSheetCustom.Builder(DashboardActivity.this, getSupportFragmentManager());
        actionSheet.setTag(AppConstants.TYPE_PRIMARY_SKILL);
        actionSheet.setCancelButtonTitle("Cancel");

        ArrayList<OrganizationData.KeysData> options = DataController.getInstance().getLogin().getOrganizationData().getSkill();

        String[] arrayStringPrimary = new String[options.size()];
        for (int index = 0; index < options.size(); index++) {
            arrayStringPrimary[index] = options.get(index).getValue();
        }

        actionSheet.setOtherButtonTitles(arrayStringPrimary);
        actionSheet.setCancelableOnTouchOutside(true);
        actionSheet.setListener(this);
        actionSheet.show();
    }

    private void showActionSheetSBU() {sheetOpen = true;
        actionSheet = new ActionSheetCustom.Builder(DashboardActivity.this, getSupportFragmentManager());
        actionSheet.setTag(AppConstants.TYPE_SBU);
        actionSheet.setCancelButtonTitle("Cancel");

        ArrayList<OrganizationData.KeysData> options = DataController.getInstance().getLogin().getOrganizationData().getSbu();

        String[] arrayStringSBU = new String[options.size()];
        for (int index = 0; index < options.size(); index++) {
            arrayStringSBU[index] = options.get(index).getValue();
        }

        actionSheet.setOtherButtonTitles(arrayStringSBU);
        actionSheet.setCancelableOnTouchOutside(true);
        actionSheet.setListener(this);
        actionSheet.show();
    }

    private void showActionSheetPrimary(String sheetType) {sheetOpen = true;
        actionSheet = new ActionSheetCustom.Builder(DashboardActivity.this, getSupportFragmentManager());
        actionSheet.setTag(sheetType);
        actionSheet.setCancelButtonTitle("Cancel");
        actionSheet.setOtherButtonTitles(optionFields);
        actionSheet.setCancelableOnTouchOutside(true);
        actionSheet.setListener(this);
        actionSheet.show();
    }

    @Override
    public void onDismiss(ActionSheetCustom actionSheet, boolean isCancel) {
        sheetOpen = false;
    }

    @Override
    public void onOtherButtonClick(ActionSheetCustom actionSheet, int index) {
        sheetOpen = false;
    //    StringOperations.shortToast("Cleck",DashboardActivity.this);



        if (actionSheet.getTag().equals(AppConstants.TYPE_PRIMARY)) {
            mSelectedPrimary = optionFields[index];

            if (!mSelectedPrimary.equals(mSelectedSecondary)) {
                tv_searchPrimary.setText(mSelectedPrimary);
                mPrimary_Selected = true;
                setOptionData(index, primaryOptions, "primary");
                selectedPrimaryValue = filterValues.get(index);
                getCustomSearch();
                tv_searchOptionPrimary.setText("Optional");
//                setOptionData(mSelectedPrimary, primaryOptions, "primary");
            } else {
                mSelectedPrimary = "Select";
                mPrimary_Selected = false;
                tv_searchPrimary.setText(mSelectedPrimary);
                StringOperations.shortToast("Already Selected in the other filter, select another field", DashboardActivity.this);
            }
            if (mPrimary_Selected && mSecondary_Selected)
                ll_filterbox2.setVisibility(View.VISIBLE);
        } else if (actionSheet.getTag().equals(AppConstants.TYPE_SECONDARY)) {
            mSelectedSecondary = optionFields[index];
            if (!mSelectedSecondary.equals(mSelectedPrimary)) {
                mSecondary_Selected = true;
                tv_searchSecondary.setText(mSelectedSecondary);
                setOptionData(index, secondaryOptions, "secondary");
                selectedSecondaryValue = filterValues.get(index);
                selectedSecondaryOptionValue = secondaryOptions.get(0).getKey();
                tv_searchOptionSecondary.setText(secondaryOptions.get(0).getValue());
                getCustomSearch();
            } else {
                mSecondary_Selected = false;
                mSelectedSecondary = "Select";
                tv_searchSecondary.setText(mSelectedSecondary);
                StringOperations.shortToast("Already Selected in the other filter, select another field", DashboardActivity.this);
            }
            if (mPrimary_Selected && mSecondary_Selected)
                ll_filterbox2.setVisibility(View.VISIBLE);
            else
                ll_filterbox2.setVisibility(View.GONE);
        } else if (actionSheet.getTag().equals(AppConstants.TYPE_PRIMARY_OPTION)) {
            selectedPrimaryOptionValue = primaryOptions.get(index).getKey();
            tv_searchOptionPrimary.setText(primaryOptions.get(index).getValue());
            getCustomSearch();
        } else if (actionSheet.getTag().equals(AppConstants.TYPE_SECONDARY_OPTION)) {
            selectedSecondaryOptionValue = secondaryOptions.get(index).getKey();
            tv_searchOptionSecondary.setText(secondaryOptions.get(index).getValue());
            getCustomSearch();
        } else if (actionSheet.getTag().equals(AppConstants.TYPE_PRIMARY_SKILL)) {
            tv_primarySkill.setText(DataController.getInstance().getLogin().getOrganizationData().getSkill().get(index).getValue());
            selectedPrimarySkill = DataController.getInstance().getLogin().getOrganizationData().getSkill().get(index).getKey();
        } else if (actionSheet.getTag().equals(AppConstants.TYPE_SBU)) {
            tv_sbu.setText(DataController.getInstance().getLogin().getOrganizationData().getSbu().get(index).getValue());
            selectedSBU = DataController.getInstance().getLogin().getOrganizationData().getSbu().get(index).getKey();
        }

        if(tv_primarySkill.getText().toString().trim().equalsIgnoreCase("Primary Skill")) {
            tv_primarySkill.setTextColor(getResources().getColor(R.color.light_gray));
         //   StringOperations.shortToast("p if",DashboardActivity.this);
        }
        else
        {
           // StringOperations.shortToast("p else",DashboardActivity.this);
            tv_primarySkill.setTextColor(getResources().getColor(android.R.color.black));
        }

        if(tv_sbu.getText().toString().trim().equalsIgnoreCase("SBU")) {

           // StringOperations.shortToast("s if",DashboardActivity.this);
            tv_sbu.setTextColor(getResources().getColor(R.color.light_gray));
        }
        else
        {
            //StringOperations.shortToast("s else",DashboardActivity.this);
            tv_sbu.setTextColor(getResources().getColor(android.R.color.black));
        }
    }

    private void setOptionData(int index, ArrayList<OrganizationData.KeysData> optionArrayList, String primaryOrSecondary) {
//        for (int index = 0; index < optionFields.length; index++) {
//            if (optionFields[index].equals(mSelected)) {
        switch (index) {
            case 0:
                optionArrayList = DataController.getInstance().getLogin().getOrganizationData().getSbu();
                break;
            case 1:
                optionArrayList = DataController.getInstance().getLogin().getOrganizationData().getSkill();
                break;
            case 2:
                optionArrayList = DataController.getInstance().getLogin().getOrganizationData().getLocation();
                break;
            case 3:
                optionArrayList = DataController.getInstance().getLogin().getOrganizationData().getDesignation();
                break;
            case 4:
                optionArrayList = DataController.getInstance().getLogin().getOrganizationData().getExperience();
                break;
            case 5:
                optionArrayList = DataController.getInstance().getLogin().getOrganizationData().getBillable();
                break;
        }
        if (primaryOrSecondary.equals("primary"))
            primaryOptions = optionArrayList;
        else if (primaryOrSecondary.equals("secondary"))
            secondaryOptions = optionArrayList;
    }

    @Override
    public void onBackPressed() {
        if(sheetOpen){
            super.onBackPressed();
            return;
        }
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        StringOperations.shortToast("Please click BACK again to exit", this);

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }
}
