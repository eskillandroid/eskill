package com.infogain.eskill.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.infogain.eskill.EskillApplication;
import com.infogain.eskill.R;
import com.infogain.eskill.constants.AppConstants;
import com.infogain.eskill.constants.DataController;
import com.infogain.eskill.model.LoginData;
import com.infogain.eskill.model.User;
import com.infogain.eskill.ssl_certificates.Sslcertificates;
import com.infogain.eskill.util.StringOperations;
import com.infogain.eskill.util.AppUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.security.cert.X509Certificate;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by inf-mobility on 28-06-2017.
 */

public class LoginActivity extends BaseActivity {

    ImageView userNameCrossImg, passwordImageView;
    RelativeLayout mainDialogRL;
    CardView loginFramWork, mainCardView;
    private EditText email_EditText;
    private EditText password_EditText;
    private Button btn_login;
    private FrameLayout fl_loader;
    public static final String MyPREFERENCES = "MyPrefs";
    ScrollView scrollView;

    @Override

    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_activity);
        getWindow().setBackgroundDrawableResource(R.drawable.loginbg);


        userNameCrossImg = (ImageView) findViewById(R.id.userNameCrossImg);
        userNameCrossImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                email_EditText.setText(null);

            }
        });
        passwordImageView = (ImageView) findViewById(R.id.passNameCrossImg);

        passwordImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                password_EditText.setText(null);
            }
        });
        loginFramWork = (CardView) findViewById(R.id.loginFramWork);

        fl_loader = (FrameLayout) findViewById(R.id.flLoader);
        scrollView = (ScrollView) findViewById(R.id.scrollView);

        loginFramWork.setCardElevation(0);

        loginFramWork.getBackground().setAlpha(28);

        AppUtils.LogAnalyticsEvent("Login Screen", FirebaseAnalytics.Event.VIEW_ITEM, getApplicationContext());

        email_EditText = (EditText) findViewById(R.id.login_email);

        password_EditText = (EditText) findViewById(R.id.login_password);


        mainDialogRL = (RelativeLayout) findViewById(R.id.mainDialogRL);
        mainDialogRL.getBackground().setAlpha(200);


        email_EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {

                loginFramWork.requestFocusFromTouch();

                if (charSequence.length() != 0) {
                    email_EditText.setError(null);
                    Log.e("TAG", "in else onTextChange" + charSequence.length());

                    userNameCrossImg.setVisibility(View.VISIBLE);
                } else {


                    userNameCrossImg.setVisibility(View.GONE);
                }


            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });


        password_EditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
                if (charSequence.length() != 0) {


                    passwordImageView.setVisibility(View.VISIBLE);
                } else {
                    passwordImageView.setVisibility(View.GONE);

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btn_login = (Button) findViewById(R.id.btn_login);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (StringOperations.isNotEmpty(email_EditText.getText().toString())) {

                    if (StringOperations.isNotEmpty(password_EditText.getText().toString())) {

                        Map<String, String> credentials = new HashMap<>();

                        credentials.put("userName", email_EditText.getText().toString());

                        credentials.put("password", password_EditText.getText().toString());


                        View view = LoginActivity.this.getCurrentFocus();
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }

                        loginService(LoginActivity.this, AppConstants.BASE_URL + AppConstants.LOGIN_URL, credentials);


                    } else {

                        StringOperations.shortToast("Please Enter Credentials", LoginActivity.this);

                        password_EditText.setError("Please enter password");
                    }
                } else {
                    email_EditText.setError("Please enter username");

                    StringOperations.shortToast("Please Enter Credentials", LoginActivity.this);
                }
            }
        });

    }


    public void loginService(Context context, final String URL, final Map<String, String> parameters) {

        // to display loading info (you can use anything else also)
//        showProgress(true, false);
        fl_loader.setVisibility(View.VISIBLE);
        btn_login.setEnabled(false);
        RequestQueue queue = EskillApplication.getInstance().getRequestQueue();


        JSONObject jsonobject_one = new JSONObject();
        try {

            jsonobject_one.put("userName", parameters.get("userName"));
            jsonobject_one.put("password", parameters.get("password"));

            Log.e("TAG", jsonobject_one.toString());

        } catch (JSONException e) {

        }

        Sslcertificates sslcertificates = new Sslcertificates();
        sslcertificates.appTrustManger();

        JsonObjectRequest stringRequest = new JsonObjectRequest(Request.Method.POST, URL, jsonobject_one, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {
                fl_loader.setVisibility(View.GONE);
                btn_login.setEnabled(true);


                LoginData loginData;
                if (response.optBoolean("success")) {
                    loginData = new LoginData(response);
                    User user = loginData.getUser();

                    String name = user.getUserdata().get(0).getFirstName() + " " + user.getUserdata().get(0).getLastName();

                    SharedPreferences.Editor editor = getSharedPreferences(MyPREFERENCES, MODE_PRIVATE).edit();

                    editor.putBoolean("ISLOGIN", true);

                    editor.putString("name", name);

                    editor.putString("response", response.toString());

                    Log.e("onrespose", "responce" + name);

                    editor.commit();

                    DataController.getInstance().setLogin(loginData);

                    Intent dashboardIntent = new Intent(LoginActivity.this, DashboardActivity.class);

                    dashboardIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(dashboardIntent);

                    finish();

                } else {

                    email_EditText.setText("");

                    password_EditText.setText("");

                    StringOperations.shortToast("Invalid login credentials", LoginActivity.this);

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                fl_loader.setVisibility(View.GONE);

                btn_login.setEnabled(true);

                Log.e("error", "error" + error);
            }
        });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                9000,

                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,

                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        queue.add(stringRequest);

    }
}
