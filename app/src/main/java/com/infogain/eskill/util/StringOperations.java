package com.infogain.eskill.util;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by Mohit.Sati on 9/8/2017.
 */

public class StringOperations {

    public static boolean isNotEmpty(String input){
        return !(input.trim().equals(""));
    }

    public static void shortToast(String message, Context context){
        Toast.makeText(context,message,Toast.LENGTH_SHORT).show();
    }
}
