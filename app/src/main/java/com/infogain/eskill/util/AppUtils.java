package com.infogain.eskill.util;

import android.content.Context;
import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by Sarvaraj.Singh on 27-09-2017.
 */

public class AppUtils {

    public static void LogAnalyticsEvent(String itemName, String eventType, Context context) {
        FirebaseAnalytics mFirebaseAnalytics = FirebaseAnalytics.getInstance(context);
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, itemName);
        mFirebaseAnalytics.logEvent(eventType, bundle);
    }
}
