package com.infogain.eskill.model;

import org.json.JSONObject;

/**
 * Created by inf-mobility on 29-06-2017.
 */

public class Data {
    String label;
    String value;
    String tooltext;

    Data(JSONObject jsonObj) {
        label = jsonObj.optString("label");
        value = jsonObj.optString("value");
        tooltext = jsonObj.optString("tooltext");
    }

    public String getLabel() {
        return label;
    }

    public String getValue() {
        return value;
    }

    public String getTooltext() {
        return tooltext;
    }
}
