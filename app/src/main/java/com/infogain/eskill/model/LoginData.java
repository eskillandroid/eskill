package com.infogain.eskill.model;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by inf-mobility on 29-06-2017.
 */

public class LoginData {
    String success;
    String message;
    String totalRecord;
    OrganizationData organizationData;
    FusionData fusionData;
    FilterData filterData;
    User user;
    public String fusionDataRaw;

    public String getFusionDataRaw() {
        return fusionDataRaw;
    }

    public String getSuccess() {
        return success;
    }

    public String getMessage() {
        return message;
    }

    public String getTotalRecord() {
        return totalRecord;
    }

    public OrganizationData getOrganizationData() {
        return organizationData;
    }

    public FusionData getFusionData() {
        return fusionData;
    }

    public FilterData getFilterData() {
        return filterData;
    }

    public User getUser() {
        return user;
    }

    public LoginData(JSONObject obj) {
        success = obj.optString("success");
        message = obj.optString("message");
        totalRecord = obj.optString("totalRecord");
        fusionDataRaw = obj.optJSONArray("fusionData").toString();
        //  JSONObject organisation = obj.optJSONObject("data");
        organizationData = new OrganizationData(obj.optJSONObject("data"));
        user = new User(obj.optJSONArray("userData"));
        fusionData = new FusionData(obj.optJSONArray("fusionData"));
        filterData = new FilterData(obj.optJSONObject("column"));
        //  organizationData.setSbu(new OrganizationData.KeysData(organisation.getJSONArray("sbu")));

    }
}
