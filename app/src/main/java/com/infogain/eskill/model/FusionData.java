package com.infogain.eskill.model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by inf-mobility on 29-06-2017.
 */

public class FusionData {
    ArrayList<Data> data = new ArrayList<>();

    FusionData(JSONArray jsonArray) {
        for (int i = 0; i <= jsonArray.length(); i++
                ) {
            try {
                data.add(new Data(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
            }
        }
    }

    public ArrayList<Data> getData() {
        return data;
    }

    public void setData(ArrayList<Data> data) {
        this.data = data;
    }
}
