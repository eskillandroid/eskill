package com.infogain.eskill.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inf-mobility on 29-06-2017.
 */

public class User {
    ArrayList<UserData> userdata = new ArrayList<>();

    User(JSONArray jsonArray) {
        for (int i = 0; i <= jsonArray.length(); i++
                ) {
            try {
                userdata.add(new UserData(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
            }
        }

    }

    public ArrayList<UserData> getUserdata() {
        return userdata;
    }


    public class UserData {
        String userName;
        String password;
        String firstName;
        String lastName;
        String designation;
        String joiningDate;
        String imgPath;

        UserData(JSONObject jsonObj) {
            userName = jsonObj.optString("userName");
            password = jsonObj.optString("password");
            firstName = jsonObj.optString("firstName");
            lastName = jsonObj.optString("lastName");
            designation = jsonObj.optString("designation");
            joiningDate = jsonObj.optString("joiningDate");
            imgPath = jsonObj.optString("imgPath");
        }

        public String getUserName() {
            return userName;
        }


        public String getPassword() {
            return password;
        }


        public String getFirstName() {
            return firstName;
        }


        public String getLastName() {
            return lastName;
        }


        public String getDesignation() {
            return designation;
        }


        public String getJoiningDate() {
            return joiningDate;
        }


        public String getImgPath() {
            return imgPath;
        }


    }
}
