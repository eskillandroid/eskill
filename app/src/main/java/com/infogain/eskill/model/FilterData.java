package com.infogain.eskill.model;

import org.json.JSONObject;

/**
 * Created by inf-mobility on 29-06-2017.
 */

public class FilterData {

    String sbu;
    String skill;
    String location;
    String designation;
    String experience;
    String billable;

    FilterData(JSONObject jsonObj) {
        sbu = jsonObj.optString("sbu");
        skill = jsonObj.optString("skill");
        location = jsonObj.optString("location");
        designation = jsonObj.optString("designation");
        experience = jsonObj.optString("experience");
        billable = jsonObj.optString("billable");

    }

    public String getSbu() {
        return sbu;
    }


    public String getSkill() {
        return skill;
    }


    public String getLocation() {
        return location;
    }


    public String getDesignation() {
        return designation;
    }


    public String getExperience() {
        return experience;
    }


    public String getBillable() {
        return billable;
    }

}
