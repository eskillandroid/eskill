package com.infogain.eskill.model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by inf-mobility on 29-06-2017.
 */

public class OrganizationData {
    ArrayList<KeysData> sbu = new ArrayList<>();
    ArrayList<KeysData> skill = new ArrayList<>();
    ArrayList<KeysData> location = new ArrayList<>();
    ArrayList<KeysData> designation = new ArrayList<>();
    ArrayList<KeysData> experience = new ArrayList<>();
    ArrayList<KeysData> billable = new ArrayList<>();

    String[] filterNames;
    ArrayList<String> filterValues;
    OrganizationData(JSONObject jsonObj) {
        filterValues = new ArrayList<>();
        filterValues.add("sbu");
        filterValues.add("skill");
        filterValues.add("location");
        filterValues.add("designation");
        filterValues.add("experience");
        filterValues.add("billable");

        filterNames = new String[]{"SBU","Primary Skill","Location","Designation","Experience","Current Billing Status"};
        JSONArray sbuArray = jsonObj.optJSONArray("sbu");
        for (int i = 0; i <= sbuArray.length(); i++
                ) {
            try {
                sbu.add(new KeysData(sbuArray.getJSONObject(i)));
            } catch (JSONException e) {
            }
        }

        JSONArray skillArray = jsonObj.optJSONArray("skill");
        for (int i = 0; i <= skillArray.length(); i++
                ) {
            try {
                skill.add(new KeysData(skillArray.getJSONObject(i)));
            } catch (JSONException e) {
            }
        }

        JSONArray locationArray = jsonObj.optJSONArray("location");
        for (int i = 0; i <= locationArray.length(); i++
                ) {
            try {
                location.add(new KeysData(locationArray.getJSONObject(i)));
            } catch (JSONException e) {
            }
        }
        JSONArray designationArray = jsonObj.optJSONArray("designation");
        for (int i = 0; i <= designationArray.length(); i++
                ) {
            try {
                designation.add(new KeysData(designationArray.getJSONObject(i)));
            } catch (JSONException e) {
            }
        }
        JSONArray experienceArray = jsonObj.optJSONArray("experience");
        for (int i = 0; i <= experienceArray.length(); i++
                ) {
            try {
                experience.add(new KeysData(experienceArray.getJSONObject(i)));
            } catch (JSONException e) {
            }
        }
        JSONArray billableArray = jsonObj.optJSONArray("billable");
        for (int i = 0; i <= billableArray.length(); i++
                ) {
            try {
                billable.add(new KeysData(billableArray.getJSONObject(i)));
            } catch (JSONException e) {
            }
        }

    }

    public String[] getFilterNames() {
        return filterNames;
    }

    public ArrayList<String> getFilterValues(){
        return filterValues;
    }

    public ArrayList<KeysData> getSbu() {
        return sbu;
    }


    public ArrayList<KeysData> getSkill() {
        return skill;
    }


    public ArrayList<KeysData> getLocation() {
        return location;
    }


    public ArrayList<KeysData> getDesignation() {
        return designation;
    }


    public ArrayList<KeysData> getExperience() {
        return experience;
    }


    public ArrayList<KeysData> getBillable() {
        return billable;
    }


    public class KeysData {
        String key;
        String value;

        KeysData(JSONObject jsonObj) {
            key = jsonObj.optString("key");
            value = jsonObj.optString("value");
        }

        public String getKey() {
            return key;
        }


        public String getValue() {
            return value;
        }


    }
}
