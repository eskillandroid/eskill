package com.infogain.eskill.constants;

/**
 * Created by inf-mobility on 03-07-2017.
 */

public class FusionChart {

    public static String get2DPieChart(String data) {
        return "<html>\n" +
                "<head>\n" +
                "    <title></title>\n" +
                "\n" +
                "</head>\n" +
                "<body>\n" +
                "<div id=\"chartContainer\"></div>\n" +
                "\n" +
                "<script src=\"file:///android_asset/fusioncharts.js\"></script>\n" +
                "<script src=\"file:///android_asset/fusioncharts.charts.js\"></script>\n" +
                "\n" +
                "<script type=\"text/javascript\">\n" +
                "FusionCharts.ready(function () {\n" +
                "    var ageGroupChart = new FusionCharts({\n" +
                "        type: 'pie2d',\n" +
                "        renderAt: 'chartContainer',\n" +
                "        width: '100%',\n" +
                "        height: '100%',\n" +
                "        dataFormat: 'json',\n" +
                "        dataSource: {\n" +
                "            \"chart\": {\n" +
                "                \"caption\": \"Designation Hierarchy\",\n" +
                "                \"subCaption\": \"Infogain Organization Level\",\n" +
                "                \"paletteColors\": \"#0075c2,#1aaf5d,#f2c500,#f45b00,#8e0000\",\n" +
                "                \"bgColor\": \"#ffffff\",\n" +
                "                \"showBorder\": \"0\",\n" +
                "                \"use3DLighting\": \"1\",\n" +
                "                \"showShadow\": \"0\",\n" +
                "                \"pieradius\":\"70\",\n" +
                "                \"enableSmartLabels\": \"1\",\n" +
                "                \"startingAngle\": \"0\",\n" +
                "                \"showPercentValues\": \"1\",\n" +
                "                \"showPercentInTooltip\": \"0\",\n" +
                "                 \"slicingDistance\": \"25\",\n" +
                "                \"decimals\": \"1\",\n" +
                "                \"captionFontSize\": \"14\",\n" +
                "                \"subcaptionFontSize\": \"14\",\n" +
                "                \"subcaptionFontBold\": \"0\",\n" +
                "                \"toolTipColor\": \"#ffffff\",\n" +
                "                \"toolTipBorderThickness\": \"0\",\n" +
                "                \"toolTipBgColor\": \"#000000\",\n" +
                "                \"toolTipBgAlpha\": \"80\",\n" +
                "                \"toolTipBorderRadius\": \"2\",\n" +
                "                \"toolTipPadding\": \"5\",\n" +
                "                \"showHoverEffect\":\"1\",\n" +
                "                \"showLegend\": \"1\",\n" +
                "                \"legendBgColor\": \"#ffffff\",\n" +
                "                \"legendBorderAlpha\": '0',\n" +
                "                \"legendShadow\": '0',\n" +
                "                \"legendItemFontSize\": '10',\n" +
                "                \"legendItemFontColor\": '#666666'\n" +
                "            },\n" +
                "            \"data\": " + data +
                "        }\n" +
                "    }).render();\n" +
                "});\n" +
                "\n" +
                "</script>\n" +
                "</body>\n" +
                "</html>";
    }

    public static String getPieChart(String data) {
        return "<html>\n" +
                "<head>\n" +
                "    <title></title>\n" +
                "\n" +
                "</head>\n" +
                "<body>\n" +
                "<div id=\"chartContainer\"></div>\n" +
                "\n" +
                "<script src=\"file:///android_asset/fusioncharts.js\"></script>\n" +
                "<script src=\"file:///android_asset/fusioncharts.charts.js\"></script>\n" +
                "\n" +
                "<script type=\"text/javascript\">\n" +
                "FusionCharts.ready(function () {\n" +
                "    var ageGroupChart = new FusionCharts({\n" +
                "        type: 'pie3d',\n" +
                "        renderAt: 'chartContainer',\n" +
                "        width: '100%',\n" +
                "        height: '100%',\n" +
                "        dataFormat: 'json',\n" +
                "        dataSource: {\n" +
                "            \"chart\": {\n" +
                "                \"caption\": \"Designation Hierarchy\",\n" +
                "                \"subCaption\": \"Infogain Organization Level\",\n" +
                "                \"paletteColors\": \"#0075c2,#1aaf5d,#f2c500,#f45b00,#8e0000\",\n" +
                "                \"bgColor\": \"#ffffff\",\n" +
                "                \"showBorder\": \"0\",\n" +
                "                \"use3DLighting\": \"1\",\n" +
                "                \"showShadow\": \"0\",\n" +
                "                \"pieradius\":\"100\",\n" +
                "                \"enableSmartLabels\": \"1\",\n" +
                "                \"startingAngle\": \"0\",\n" +
                "                \"showPercentValues\": \"1\",\n" +
                "                \"showPercentInTooltip\": \"0\",\n" +
                "                 \"slicingDistance\": \"25\",\n" +
                "                \"decimals\": \"1\",\n" +
                "                \"captionFontSize\": \"14\",\n" +
                "                \"subcaptionFontSize\": \"14\",\n" +
                "                \"subcaptionFontBold\": \"0\",\n" +
                "                \"toolTipColor\": \"#ffffff\",\n" +
                "                \"toolTipBorderThickness\": \"0\",\n" +
                "                \"toolTipBgColor\": \"#000000\",\n" +
                "                \"toolTipBgAlpha\": \"80\",\n" +
                "                \"toolTipBorderRadius\": \"2\",\n" +
                "                \"toolTipPadding\": \"5\",\n" +
                "                \"showHoverEffect\":\"1\",\n" +
                "                \"showLegend\": \"1\",\n" +
                "                \"legendBgColor\": \"#ffffff\",\n" +
                "                \"legendBorderAlpha\": '0',\n" +
                "                \"legendShadow\": '0',\n" +
                "                \"legendItemFontSize\": '10',\n" +
                "                \"legendItemFontColor\": '#666666'\n" +
                "            },\n" +
                "            \"data\": " + data +
                "        }\n" +
                "    }).render();\n" +
                "});\n" +
                "\n" +
                "</script>\n" +
                "</body>\n" +
                "</html>";
    }


    String chart = "<html>\n" +
            "<head>\n" +
            "<title>My first chart using FusionCharts Suite XT</title>\n" +
            "<script type=\"text/javascript\" src=\"http://static.fusioncharts.com/code/latest/fusioncharts.js\"></script>\n" +
            "<script type=\"text/javascript\" src=\"http://static.fusioncharts.com/code/latest/themes/fusioncharts.theme.fint.js?cacheBust=56\"></script>\n" +
            "<script type=\"text/javascript\">\n" +
            "  FusionCharts.ready(function(){\n" +
            "    var fusioncharts = new FusionCharts({\n" +
            "    type: 'pyramid',\n" +
            "    renderAt: 'chart-container',\n" +
            "    id: 'pyramid-chart',\n" +
            "    width: '500',\n" +
            "    height: '400',\n" +
            "    dataFormat: 'json',\n" +
            "    dataSource: {\n" +
            "        \"chart\": {\n" +
            "            \"theme\": \"fint\",\n" +
            "            \"caption\": \"The Global Wealth Pyramid\",\n" +
            "            \"captionOnTop\": \"0\",\n" +
            "            \"captionPadding\": \"25\",\n" +
            "            \"alignCaptionWithCanvas\": \"1\",\n" +
            "            \"subcaption\": \"Credit Suisse 2013\",\n" +
            "            \"subCaptionFontSize\": \"12\",\n" +
            "            \"borderAlpha\": \"20\",\n" +
            "            \"is2D\": \"1\",\n" +
            "            \"bgColor\": \"#ffffff\",\n" +
            "            \"showValues\": \"1\",\n" +
            "            \"numberPrefix\": \"$\",\n" +
            "            \"numberSuffix\": \"M\",\n" +
            "            \"plotTooltext\": \"$label of world population is worth USD $value tn \",\n" +
            "            \"showPercentValues\": \"1\",\n" +
            "            \"chartLeftMargin\": \"40\"\n" +
            "        },\n" +
            "        \"data\":" + DataController.getInstance().getLogin().fusionDataRaw + "\n" +
            "    }\n" +
            "}\n" +
            ");\n" +
            "    fusioncharts.render();\n" +
            "});\n" +
            "</script>\n" +
            "</head>\n" +
            "<body>\n" +
            "  <div id=\"chart-container\"></div>\n" +
            "</body>\n" +
            "</html>";

    String ss = "<html>\n" +
            "<head>\n" +
            "    <title>My first chart using FusionCharts Suite XT</title>\n" +
            "    <script type=\"text/javascript\" src=\"file:///android_asset/fusioncharts.js\"></script>\n" +
            "    <script type=\"text/javascript\" src=\"file:///android_asset/fusioncharts.charts.js\"></script>\n" +
            "    <script type=\"text/javascript\" src=\"file:///android_asset/fusioncharts.theme.fint.js\"></script>\n" +
            "    <script type=\"text/javascript\">\n" +
            "  FusionCharts.ready(function(){\n" +
            "    var revenueChart = new FusionCharts({\n" +
            "        \"type\": \"column2d\",\n" +
            "        \"renderAt\": \"chartContainer\",\n" +
            "        \"width\": \"260\",\n" +
            "        \"height\": \"300\",\n" +
            "        \"dataFormat\": \"json\",\n" +
            "        \"dataSource\":  {\n" +
            "          \"chart\": {\n" +
            "            \"caption\": \"Quarterly revenue for last year\",\n" +
            "            \"subCaption\": \"Harry's SuperMart\",\n" +
            "             \"palettecolors\":\"#2874A6\",\n" +
            "             \"useplotgradientcolor\":\"0\",\n" +
            "            \"yAxisName\": \"Revenues (In USD)\",\n" +
            "            \"theme\": \"fint\",\n" +
            "            \"showvalues\":\"0\",\n" +
            "            \"yaxismaxvalue\":\"40000\",\n" +
            "            \"yaxisminvalue\":\"0\"\n" +
            "         },\n" +
            "         \"data\": [\n" +
            "            {\n" +
            "               \"label\": \"Q1\",\n" +
            "               \"value\": \"16000\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"label\": \"Q2\",\n" +
            "               \"value\": \"25300\"\n" +
            "            },\n" +
            "            {\n" +
            "               \"label\": \"Q3\",\n" +
            "               \"value\": \"10140\"\n" +
            "            },\n" +
            "             {\n" +
            "               \"label\": \"Q4\",\n" +
            "               \"value\": \"19140\"\n" +
            "            }\n" +
            "          ]\n" +
            "      }\n" +
            "\n" +
            "  });\n" +
            "revenueChart.render();\n" +
            "})\n" +
            "</script>\n" +
            "</head>\n" +
            "<body>\n" +
            "<div id=\"chartContainer\">FusionCharts XT will load here!</div>\n" +
            "</body>\n" +
            "</html>";
}
