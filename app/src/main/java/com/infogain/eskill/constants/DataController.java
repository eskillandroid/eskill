package com.infogain.eskill.constants;

import com.infogain.eskill.model.LoginData;

/**
 * Created by inf-mobility on 29-06-2017.
 */

public class DataController {
  static   DataController controller = new DataController();
    private DataController()
    {

    }
    public static DataController getInstance()
    {
        if(controller!=null)
            return controller;
        else
            return new DataController();
    }

    LoginData login;

    public LoginData getLogin() {
        return login;
    }

    public void setLogin(LoginData login) {
        this.login = login;
    }
}
