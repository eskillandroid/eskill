package com.infogain.eskill.constants;

/**
 * Created by inf-mobility on 28-06-2017.
 */

public class AppConstants {
//    public static final String BASE_URL = "http://172.18.65.154:8085/eSkill/webservice/";

    public static final String BASE_URL = "https://eskill.infogain.com/webservice/";

    public static final String LOGIN_URL = "login/verify/";

    public static final String GRAPH_URL = "search/getGraphData";

    public static final String PRIMARY_SEARCH_URL = "search/getCustomGraphData";

    public static final String GRAPH_ORG_SEARCH_URL = "graph/getGraphData";

    public static final String TYPE_PRIMARY = "primary selection";

    public static final String TYPE_PRIMARY_OPTION = "primary option";

    public static final String TYPE_SECONDARY = "secondary selection";

    public static final String TYPE_SECONDARY_OPTION = "secondary option";

    public static final String TYPE_PRIMARY_SKILL = "primary skill";

    public static final String TYPE_SBU = "sbu";

}
