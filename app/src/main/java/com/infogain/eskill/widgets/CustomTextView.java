package com.infogain.eskill.widgets;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

/**
 * Created by Upender.Vijay on 11/30/2017.
 */

public class CustomTextView extends  TextView {


    public CustomTextView(Context context) {
        super(context);
       // Typeface face=Typeface.createFromAsset(context.getAssets(), "font/arcon_regular.otf");
       // this.setTypeface(face);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    //    Typeface face=Typeface.createFromAsset(context.getAssets(), "font/arcon_regular.otf");
      //  this.setTypeface(face);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
      //  Typeface face=Typeface.createFromAsset(context.getAssets(), "font/arcon_regular.otf");
      //  this.setTypeface(face);
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);


    }

}