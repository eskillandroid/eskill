package com.infogain.eskill.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.infogain.eskill.R;
import com.infogain.eskill.model.OrganizationData;

import java.util.ArrayList;

/**
 * Created by inf-mobility on 03-07-2017.
 */

public class FilterAdapter extends BaseAdapter {


        Context context;
        int flags[];
        String[] countryNames;
        LayoutInflater inflter;
    ArrayList<OrganizationData.KeysData> Data;

        public FilterAdapter(Context applicationContext,ArrayList<OrganizationData.KeysData> data) {
            this.context = applicationContext;
            this.Data = data;
            inflter = (LayoutInflater.from(applicationContext));
        }

        @Override
        public int getCount() {
            return Data.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = inflter.inflate(R.layout.spinner_items, null);
//        ImageView icon = (ImageView) view.findViewById(R.id.imageView);
        TextView names = (TextView) view.findViewById(R.id.textView);
//        icon.setImageResource(flags[i]);
       names.setText(Data.get(position).getValue());
        return view;
    }


    }

